%%% @doc Implementation of commands the user can execute by sending a
%%% sequence that starts with `$\\'.
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(chat_command).

%% API
-export([exec/1
        ]).

-include("chat_server.hrl").

%%------------------------------------------------------------------------------
%% Macros
%%------------------------------------------------------------------------------

-define(is_whitespace(C), (C =:= 32 orelse (C >= 9 andalso C =< 13))).
%% Test a character for being an (ASCII) whitespace. Can be used in
%% guards.

-define(reply(Msg), {reply, <<"\e[36;1m", Msg, "\e[0m\r">>}).
%% Create a reply action that prints the message in gray.

%%------------------------------------------------------------------------------
%% Types
%%------------------------------------------------------------------------------

-type action() :: {set_name, Name :: binary()}
                | {reply,    Msg  :: iodata()}
                  .
%% Possible outcomes of executing a command.

-export_type([action/0
             ]).

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Execute a command and return some actions to perform.
-spec exec(Sequence :: binary()) -> {ok, [action()]}.
exec(Sequence) ->
    Actions =
        case split_next_token(trim(Sequence)) of
            {<<"\\help">>, <<>>} ->
                exec_help();
            {<<"\\name">>, Name} when Name =/= <<>> ->
                exec_name(Name);
            {<<"\\stats">>, <<>>} ->
                exec_stats();
                  _Other ->
                [?reply("No such command\r"
                        "Type \\help to see the list of available commands")]
        end,
    {ok, Actions}.

%%------------------------------------------------------------------------------
%% Internal helper functions
%%------------------------------------------------------------------------------

%% @doc Execute the help command
-spec exec_help() -> [action()].
exec_help() ->
    [?reply("Available commands:\r"
            "\r"
            "* \\name NAME \tchange your nick name\r"
            "* \\stats     \tget performance statistics\r"
            "* \\help      \tprint this help message")].

%% @doc Execute the name command that changes the nick name of the
%% user.
-spec exec_name(Name :: binary()) -> [action()].
exec_name(Name) ->
    case [C || <<C>> <= Name, C < 32] of
        [] ->
            %% Valid name: there are no control characters in it
            [?reply(<<"Your name is from now on ", Name/binary>>/binary),
             {set_name, Name}];
        _CtrlChars ->
            [?reply("Your name cannot contain control characters")]
    end.

%% @doc Execute the stats command that prints the exometer statistics
%% to the user.
-spec exec_stats() -> [action()].
exec_stats() ->
    Msg = [[io_lib:format("\r* ~p", [Metric]),
            [io_lib:format("\r  ~p = ~p", [Name, Value])
             || {Name, Value} <- DataPoints]
           ]
           || {Metric, _Type, enabled} <- exometer:find_entries(['_']),
              {_Metric, DataPoints} <- exometer:get_values(Metric)],
    [?reply(<<"Statistics",
              (unicode:characters_to_binary(Msg))/binary
            >>/binary)].

%% @doc Return the first token and the rest of text (except for the
%% delimiting whitespace). Tokens are separated by whitespace, and the
%% input must be already trimmed.
-spec split_next_token(Text :: binary()) -> {Token :: binary(),
                                               Rest  :: binary()}.
split_next_token(Text) ->
    WS = [<<C>> || C <- [32] ++ lists:seq(9, 13)],
    case binary:match(Text, WS) of
        nomatch ->
            {Text, <<>>};
        {I, 1} ->
            {binary:part(Text, 0, I),
             ltrim(binary:part(Text, I, byte_size(Text) - I))}
    end.

%% @doc Remove whitespace from both ends of a binary.
-spec trim(Text :: binary()) -> binary().
trim(Text) ->
    rtrim(ltrim(Text)).

%% @doc Remove whitespace from the beginning of a binary.
-spec ltrim(Text :: binary()) -> binary().
ltrim(<<C, Text/binary>>) when ?is_whitespace(C) ->
    ltrim(Text);
ltrim(Text) ->
    Text.


%% @doc Remove whitespace from the end of a binary.
rtrim(<<>>) ->
    <<>>;
rtrim(Text) ->
    S = byte_size(Text) - 1,
    case Text of
        <<T:S/binary, C>> when ?is_whitespace(C) ->
            rtrim(T);
        _ ->
            Text
    end.
