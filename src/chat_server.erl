%%% @doc The application module of the chat server.
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(chat_server).

%% API
-export([start/0,
         stop/0
        ]).

-behaviour(application).
-export([start/2,
         stop/1
        ]).

-include("chat_server.hrl").

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Start the application. This function is most useful when starting {@link
%% chat_server} from the command line: `erl -s chat_server'.
-spec start() -> ok | {error, _}.
start() ->
    {ok, _} = application:ensure_all_started(chat_server, permanent),
    ok.

%% @doc Stop the application.
-spec stop() -> ok | {error, _}.
stop() ->
    application:stop(chat_server).

%%------------------------------------------------------------------------------
%% application callbacks
%%------------------------------------------------------------------------------

%% @private @doc Start the application's supervision tree.
-spec start(StartType :: application:start_type(),
            StartArgs :: term()) -> {ok, pid()} | {error, _}.
start(_StartType, _StartArgs) ->
    chat_server_sup:start_link().

%% @private @doc Clean up after the application has been stopped.
-spec stop(State :: term()) -> ok.
stop(_State) ->
	ok.
