%%% @doc Worker for delivering messages to users.
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(msg_delivery).

%% API
-export([deliver/2
        ]).

-include("chat_server.hrl").

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Deliver a message coming from a socket to all other active
%% sessions asynchronously. The delivery worker will be linked to the
%% calling process.
-spec deliver(SourceSocket :: any(), Msg :: iodata()) -> pid().
deliver(SourceSocket, Msg) ->
    spawn_link(fun () -> sync_deliver(SourceSocket, Msg) end).

%%------------------------------------------------------------------------------
%% Internal helper functions
%%------------------------------------------------------------------------------

%% @doc Synchronously deliver a message coming from a socket to all
%% other active sessions.
-spec sync_deliver(SourceSocket :: any(), Msg :: iodata()) -> _.
sync_deliver(SourceSocket, Msg) ->
    Fun = fun (Transport, Socket, Pid) when Socket =/= SourceSocket ->
                  send_msg(Transport, Socket, Pid, Msg);
              (_Transport, _Socket, _Pid) ->
                  %% Do not deliver the message back to the original
                  %% source
                  ok
          end,
    {T, _} = timer:tc(session_registry, foreach, [Fun]),
    exometer:update(?msg_delivery, T).

%% @doc Send a message to a socket. Log any errors, but do not crash.
-spec send_msg(Transport :: module(),
               Socket    :: any(),
               Pid       :: pid(),
               Msg       :: iodata()) -> _.
send_msg(Transport, Socket, Pid, Msg) ->
    try Transport:send(Socket, Msg) of
        ok ->
            ok;
        {error, Reason} ->
            lager:error("error writing to socket ~p (session ~p): ~p",
                        [Socket, Pid, Reason]),
            %% The most common error: the socket is closed but not yet
            %% removed from the registry (e.g. because the {@link
            %% chat_session} crashed). In this case remove the entry
            %% here.
            Reason =:= closed andalso
                begin
                    lager:debug("removing dead socket ~p (session ~p)",
                                [Socket, Pid]),
                    exometer:update(?conn_err, +1),
                    exometer:update(?conn_cnt, -1),
                    session_registry:remove_session(Socket)
                end
    catch
        Class:Error ->
            lager:error("~p in writing to socket ~p (session ~p): ~p",
                        [Class, Socket, Pid, Error])
    end.
