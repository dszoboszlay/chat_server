%%% @doc The supervisor of the chat server.
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(chat_server_sup).

%% API
-export([start_link/0
        ]).

-behaviour(supervisor).
-export([init/1
        ]).

-include("chat_server.hrl").

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Start the top supervisor tree of the application.
-spec start_link() -> {ok, pid()} | {error, _}.
start_link() ->
    supervisor:start_link({local, chat_server}, ?MODULE, top).

%%------------------------------------------------------------------------------
%% supervisor callbacks
%%------------------------------------------------------------------------------

%% @private @doc Initialise the supervisor.
-spec init(top) -> {ok,
                    {{supervisor:strategy(), pos_integer(), non_neg_integer()},
                     [supervisor:child_spec()]}}.
init(top) ->
    Children = [worker(session_registry),
                worker(chat_listener)],
    {ok, {{rest_for_one, 1, 5}, Children}}.

%%------------------------------------------------------------------------------
%% Internal helper functions
%%------------------------------------------------------------------------------

%% @doc Create a child spec for a worker module with a `start_link/0'
%% function.
-spec worker(Module :: module()) -> supervisor:child_spec().
worker(Module) ->
    {Module,
     {Module, start_link, []},
     permanent, 5000, worker, [Module]
    }.
