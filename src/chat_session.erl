%%% @doc Implementation of a user session process. A user session is
%%% modelled with a `ranch_protocol'. It is actually a very basic,
%%% line based protocol, so the main task of the session process is
%%% rather to hold the user's state (like her nick name) and take care
%%% of dispatching messages to other connected users.
%%%
%%% For this reason the {@link chat_session} registers itself in the
%%% {@link session_registry} so that other users will be able to
%%% discover it, and uses {@link msg_delivery} to dispatch messages.
%%%
%%% Messages are kept as a binary because this representation is both
%%% compact and efficient to send in messages due to the reference
%%% counting.
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(chat_session).

-behaviour(ranch_protocol).
-export([start_link/4
        ]).

%% proc_lib & sys callbacks
-export([init/5,
         system_continue/3,
         system_terminate/4,
         system_code_change/4,
         system_get_state/1,
         system_replace_state/2,
         system_format/3
        ]).

-include("chat_server.hrl").

%%------------------------------------------------------------------------------
%% Constants & macros
%%------------------------------------------------------------------------------

-define(N, 10).
%% The `{active, N}' parameter for the socket.

-define(log(Level, Event, Format, Args, Debug, State),
        begin
            lager:Level([{user, State#state.name}], Format, Args),
            sys:handle_debug(Debug,
                             fun ?MODULE:system_format/3,
                             [Format | Args],
                             Event)
        end
       ).
%% A macro for logging messages with both `lager' and
%% `sys:handle_debug/4'.

-define(eval_and_loop(Eval, Parent, Debug, State),
        try Eval of
            __S ->
                system_continue(Parent, Debug, __S)
        catch
            __Class:__Error ->
                __D = ?log(error, error,
                           "~s in session process: ~p at ~p",
                           [__Class, __Error, erlang:get_stacktrace()],
                           Debug, State),
                system_terminate({error, __Error}, Parent, __D, State)
        end
       ).
%% A macro for the safely handling a message and returning to the
%% message loop.

%%------------------------------------------------------------------------------
%% Types
%%------------------------------------------------------------------------------

-type session_opts() :: [session_opt()].
%% The list of options the chat session ranch protocol takes.

-type session_opt() :: {debug,      [debug_opt()]}
                     | {eol,        binary()}
                     | {buff_limit, pos_integer()}
                       .
%% Options supported by the chat session process.
%% <dl>
%% <dt>`buff_limit'</dt>
%% <dd>A soft limit on the number of unsent message bytes read from
%%     the socket. Once the number of buffered bytes exceeds this
%%     limit the socket is put into passive mode. However the actual
%%     buffer may grow larger than this limit if the socket has
%%     already sent more data messages. Those bytes are already in the
%%     message queue however, so moving them from there to the buffer
%%     will hardly affect the total memory consumption.</dd>
%% </dl>

-type debug_opt() :: trace
                   | log
                   | {log, pos_integer()}
                   | statistics
                   | {log_to_file, file:name()}
                   | {install, {sys:dbg_fun(), term()}}
                     .
%% Type of standard sys debug options.

-record(state, {name       = <<>>,              % The name of the user
                transport,                      % Ranch transport module
                socket,                         % Socket
                eol,                            % End of line match pattern
                data       = [],                % A not-yet finished line
                buff       = [],                % Messages not yet delivered
                buff_limit,                     % Soft size limit on `buff'
                worker                          % Pid of the delivery worker
               }).
%% The state of the session process.

-type state() :: #state{name       :: binary(),
                        transport  :: module(),
                        socket     :: any(),
                        eol        :: binary:cp(),
                        data       :: iodata(),
                        buff       :: iodata(),
                        buff_limit :: pos_integer(),
                        worker     :: pid() | undefined
                       }.
%% The type of the session process' state. It does not include the
%% common OTP process parameters, parent and debug, as they need to be
%% passed to `sys' callback as separate arguments anyway.

-export_type([debug_opt/0,
              session_opt/0,
              session_opts/0
             ]).

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Start a chat session process.
-spec start_link(Ref       :: ranch:ref(),
                 Socket    :: any(),
                 Transport :: module(),
                 Opts      :: session_opts()) -> {ok, pid()}.
start_link(Ref, Socket, Transport, Opts) ->
    Args = [self(), Ref, Socket, Transport, Opts],
    Pid = proc_lib:spawn_link(?MODULE, init, Args),
    {ok, Pid}.

%%------------------------------------------------------------------------------
%% proc_lib & sys callbacks
%%------------------------------------------------------------------------------

%% @private @doc Initialise a new chat session process.
-spec init(Parent    :: pid(),
           Ref       :: ranch:ref(),
           Socket    :: any(),
           Transport :: module(),
           Opts      :: session_opts()) -> no_return().
init(Parent, Ref, Socket, Transport, Opts) ->
    %% Look up options
    D0    = sys:debug_options(proplists:get_value(debug, Opts, [])),
    Eol   = binary:compile_pattern(proplists:get_value(eol, Opts, <<$\r>>)),
    Limit = proplists:get_value(buff_limit, Opts, 64 * 1024),
    %% Send ack to ranch, so we can start using the socket
    ok = ranch:accept_ack(Ref),
    %% Initialise the process:
    %% - trap exits
    %% - set socket options
    %% - register session
    %% - set default name
    process_flag(trap_exit, true),
    ok = Transport:setopts(Socket, [{active, ?N}]),
    session_registry:add_session(Transport, Socket, self()),
    S0 = #state{transport  = Transport,
                socket     = Socket,
                eol        = Eol,
                buff_limit = Limit},
    S1 = set_default_name(S0),
    D1 = ?log(debug, init,
              "new connection", [], D0, S1),
    exometer:update(?conn_cnt, 1),
    system_continue(Parent, D1, S1).

%% @private @doc The message loop of the session process.
-spec system_continue(Parent :: pid(),
                      Debug  :: [sys:dbg_opt()],
                      State  :: state()) -> no_return().
system_continue(Parent, D0, S0 = #state{transport = Transport,
                                        socket    = Socket,
                                        worker    = Pid}) ->
    {OK, Closed, Error} = Transport:messages(),
    receive
        {system, From, Msg} ->
            sys:handle_system_msg(Msg, From, Parent, ?MODULE, D0, S0);
        {'EXIT', Pid, normal} = Msg ->
            %% Delivery worker finished
            D1 = ?log(debug, {in, Msg},
                      "delivery worker finished", [], D0, S0),
            ?eval_and_loop(handle_delivery_done(S0), Parent, D1, S0);
        {'EXIT', _, Reason} = Msg ->
            D1 = ?log(debug, {in, Msg},
                      "linked process terminated: ~p", [Reason], D0, S0),
            if Reason =:= normal ->
                    %% Ignore the event
                    system_continue(Parent, D1, S0);
               true ->
                    %% A linked process crashed: let's terminate!
                    system_terminate(Reason, Parent, D1, S0)
            end;
        {OK, Socket, Data} ->
            %% Got more data
            D1 = ?log(debug, {in, OK},
                      "received data: ~p", [Data], D0, S0),
            ?eval_and_loop(handle_data(Data, S0), Parent, D1, S0);
        {Closed, Socket} ->
            %% Socket is closed, let's terminate
            D1 = ?log(debug, {in, Closed},
                      "connection closed", [], D0, S0),
            system_terminate(normal, Parent, D1, S0);
        {Error, Socket, Reason} ->
            %% Socket error, let's terminate
            D1 = ?log(error, {in, Error},
                      "socket error: ~p", [Reason], D0, S0),
            system_terminate({error, Reason}, Parent, D1, S0);
        {tcp_passive, Socket} ->
            %% The socket went back to passive mode (unfortunately the
            %% ranch transport behaviour doesn't contain a tag for
            %% this message, we must hardcode `tcp_passive')
            D1 = ?log(debug, {in, passive},
                      "socket goes to passive mode", [], D0, S0),
            ?eval_and_loop(handle_passive_socket(S0), Parent, D1, S0);
        Msg ->
            %% Unexpected message
            D1 = ?log(warning, {in, Msg},
                      "unexpected message: ~p", [Msg], D0, S0),
            system_continue(Parent, D1, S0)
    end.

%% @private @doc Terminate the session process. This function cleans
%% up after the connection by closing the socket, therefore it should
%% be always called regardless of the reason of termination.
-spec system_terminate(Reason :: term(),
                       Parent :: pid(),
                       Debug  :: [sys:dbg_opt()],
                       State  :: state()) -> no_return().
system_terminate(Reason, _Parent, D0, S0 = #state{transport = Transport,
                                                  socket    = Socket}) ->
    D1 = try Transport:close(Socket) of
             ok ->
                 D0;
             Error ->
                 ?log(error, error,
                      "error closing socket: ~p", [Error], D0, S0)
         catch
             Class:Error ->
                 ?log(error, error,
                      "~s in closing socket: ~p at ~p",
                      [Class, Error, erlang:get_stacktrace()], D0, S0)
         end,
    session_registry:remove_session(Socket),
    exometer:update(?conn_cnt, -1),
    ?log(debug, terminate,
         "terminated", [], D1, S0),
    exit(Reason).

%% @private @doc Perform a code change. There are no older versions of
%% this module from which an upgrade would be possible, so this
%% function is just an empty stub.
-spec system_code_change(State  :: state(),
                         Module :: module(),
                         OldVsn :: term(),
                         Extra  :: term()) -> {ok, state()}.
system_code_change(State, _Module, _OldVsn, _Extra) ->
    {ok, State}.

%% @private @doc Get the state of the module from the parameter passed
%% to `sys:handle_system_msg/6'. This is actually a no-op in this
%% module's case.
-spec system_get_state(State :: state()) -> {ok, state()}.
system_get_state(State) ->
    {ok, State}.

%% @private @doc Apply a transformation function on the process'
%% state.
-spec system_replace_state(StateFun :: fun((state()) -> state()),
                           State    :: state()) -> {ok, state(), state()}.
system_replace_state(StateFun, S0) ->
    S1 = StateFun(S0),
    {ok, S1, S1}.

%% @private @doc A formatter function for the system debug messages
%% generated by this module. By convention the `Extra' argument will
%% always hold a format string followed by its arguments.
-spec system_format(Device :: io:device() | file:io_device(),
                    Event  :: sys:system_event(),
                    Extra  :: [term(), ...]) -> ok.
system_format(Device, _Event, [Format | Args]) ->
    io:format(Device, Format, Args).

%%------------------------------------------------------------------------------
%% Helper functions
%%------------------------------------------------------------------------------

%% @doc Handle the socket going into passive mode. Unless the socket
%% needs to be blocked it is immediately put back into active mode.
-spec handle_passive_socket(State :: state()) -> state().
handle_passive_socket(State = #state{transport = Transport,
                                     socket    = Socket}) ->
    is_blocking(State) orelse Transport:setopts(Socket, [{active, ?N}]),
    State.

%% @doc Handle the termination of the previous delivery. Unblock the
%% socket when necessarry and deliver any buffered messages.
-spec handle_delivery_done(State :: state()) -> state().
handle_delivery_done(State = #state{socket    = Socket,
                                    transport = Transport}) ->
    is_blocking(State) andalso
        %% It is time to unblock the socket
        Transport:setopts(Socket, [{active, ?N}]),
    %% In case we already have some messages buffered deliver them
    maybe_deliver(State#state{worker = undefined}).

%% @doc Handle new data sent by the user.
-spec handle_data(Data :: binary(), State :: state()) -> state().
handle_data(Data, State) ->
    handle_data(Data, State, 0).

%% @doc Handle new data sent by the user with sequence counting.
-spec handle_data(Data  :: binary(),
                  State :: state(),
                  Cnt   :: non_neg_integer()) -> state().
handle_data(NewData,
            S0 = #state{data = OldData,
                        eol  = Eol},
            Cnt) ->
    case binary:match(NewData, Eol) of
        {Offset, 1} ->
            N = Offset + 1,
            L = byte_size(NewData),
            S1 = handle_sequence(append(OldData, binary:part(NewData, 0, N)),
                                 S0#state{data = []}),
            if N =:= L ->
                    %% The entire packet has been used
                    exometer:update(?msg_per_sec, Cnt + 1),
                    maybe_deliver(S1);
               true ->
                    %% There may be additional lines in the remaining part
                    handle_data(binary:part(NewData, N, L - N), S1, Cnt + 1)
            end;
        nomatch ->
            Cnt > 0 andalso exometer:update(?msg_per_sec, Cnt),
            maybe_deliver(S0#state{data = append(OldData, NewData)})
    end.

%% @doc Handle a complete sequence entered by the user. If the user
%% enters a command (a sequence starting with `$\\') it will be
%% processed, otherwise the message is buffered for delivery.
-spec handle_sequence(Sequence :: iodata(), State :: state()) -> state().
handle_sequence(Seq,
                S0 = #state{name      = Name,
                            buff      = Buff,
                            transport = Transport,
                            socket    = Socket}) ->
    case is_command(Seq) of
        true ->
            %% Handle a special command
            {ok, Actions} = chat_command:exec(iolist_to_binary(Seq)),
            lists:foldl(fun perform_action/2, S0, Actions);
        false ->
            %% Buffer the message for delivery
            WasNotBlocking = not is_blocking(S0),
            S1 = S0#state{buff = append(Buff, [Name, Seq])},
            WasNotBlocking andalso is_blocking(S1) andalso
            %% It is time to block the socket
                Transport:setopts(Socket, [{active, false}]),
            S1
    end.

%% @doc Check whether the socket needs to be blocked (put into passive
%% mode) to prevent it from flooding the memory while a delivery
%% worker is running.
-spec is_blocking(State :: state()) -> boolean().
is_blocking(#state{worker     = Worker,
                   buff       = Buff,
                   buff_limit = Limit}) ->
    Worker =/= undefined andalso iolist_size(Buff) >= Limit.

%% @doc Decide whether a sequence is a command. This requires checking
%% the first character of the iodata structure.
-spec is_command(Sequence :: iodata()) -> boolean().
is_command(<<$\\, _/binary>>) -> 
    true;
is_command([$\\ | _]) ->
    true;
is_command([Txt | _]) -> 
    is_command(Txt);
is_command(_Txt) -> 
    false.

%% @doc Start a delivery worker unless one is already running.
-spec maybe_deliver(State :: state()) -> state().
maybe_deliver(State = #state{worker    = undefined,
                             socket    = Socket,
                             buff      = Buff}) when Buff =/= [] ->
    State#state{worker = msg_delivery:deliver(Socket, Buff),
                buff   = []};
maybe_deliver(State) ->
    %% A delivery is already in progress
    State.

%% @doc Append to an iodata structure. The goal is that when starting
%% from nil appending any representation of the empty text should
%% still result in nil.
-spec append(OldValue :: iodata(),
             NewValue :: binary() | [binary(), ...]) -> iodata().
append(OldValue, <<>>) ->
    OldValue;
append([], NewValue) ->
    NewValue;
append(OldValue, NewValue) ->
    [OldValue | NewValue].

%% @doc Perform a chat command action.
-spec perform_action(Action :: chat_command:action(),
                     State :: state()) -> state().
perform_action({set_name, Name}, State) ->
    set_name(Name, State);
perform_action({reply, Msg},
             State = #state{transport = Transport,
                            socket    = Socket}) ->
    Transport:send(Socket, Msg),
    State.

%% @doc Change the name of the user in the state. The name provided in
%% the argument will be wrapped into some ANSI escape code sequences
%% that would render the name in bold green on terminal emulators. A
%% space is also appended to the name, so it will be nicely separated
%% from the actual chat message.
-spec set_name(Name :: binary(), State :: state()) -> state().
set_name(Name, State) ->
    ColouredName = <<"\e[32;1m", Name/binary, "\e[0m ">>,
    State#state{name = ColouredName}.

%% @doc Set a default name for the user based on her network address.
-spec set_default_name(State :: state()) -> state().
set_default_name(State = #state{transport = Transport,
                                socket    = Socket}) ->
    Name =
        try Transport:peername(Socket) of
            {ok, {IP, Port}} ->
                [format_ip(IP), $:, integer_to_binary(Port)];
            {error, _} ->
                io_lib:format("~s/~p", [Transport, Socket])
        catch
            _:_ ->
                io_lib:format("~s/~p", [Transport, Socket])
        end,
    set_name(unicode:characters_to_binary(Name), State).

%% @doc Convert an IP address to a user readable string.
-spec format_ip(IP :: inet:ip_address()) -> string().
format_ip(IP) when is_tuple(IP) ->
    {Base, SepChar} =
        case size(IP) of
            4 -> {10, $.};                      % IPv4 address
            8 -> {16, $:}                       % IPv6 address
        end,
    string:join([string:to_lower(integer_to_list(N, Base))
                 || N <- tuple_to_list(IP)],
                [SepChar]).
