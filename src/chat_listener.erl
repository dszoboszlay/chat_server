%%% @doc The server responsible for starting and stopping ranch on the
%%% chat server's TCP port.
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(chat_listener).

%% API
-export([start_link/0
        ]).

-behaviour(gen_server).
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3
        ]).

-include("chat_server.hrl").

%%------------------------------------------------------------------------------
%% Constants
%%------------------------------------------------------------------------------

-define(ref, ?MODULE).
%% The ranch listener's id.

%%------------------------------------------------------------------------------
%% Types
%%------------------------------------------------------------------------------

-type state() :: no_value.
%% The state of the server.

-type from() :: {pid(), _}.
%% The sender of a call request.

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Start the process responsible for opening the TPC port.
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, no_value, []).

%%------------------------------------------------------------------------------
%% gen_server callbacks
%%------------------------------------------------------------------------------

%% @private @doc Initialise the server.
-spec init(Args :: no_value) -> {ok, state(), hibernate}.
init(_Args) ->
    process_flag(trap_exit, true),
    %% Look up ranch settings from the environment
    {ok, App} = application:get_application(),
    Acceptors = application:get_env(App, acceptors,  1000),
    Ip        = application:get_env(App, ip,         {0, 0, 0, 0}),
    Port      = application:get_env(App, port,       6667),
    Debug     = application:get_env(App, debug,      []),
    Eol       = application:get_env(App, eol,        <<$\r>>),
    Limit     = application:get_env(App, buff_limit, 64 * 1024),
    %% Start ranch
    {ok, _Pid} = ranch:start_listener(
                   ?ref,
                   Acceptors,
                   ranch_tcp,
                   [{port, Port}, {ip, Ip}],
                   chat_session,
                   [{debug,      Debug},
                    {eol,        unicode:characters_to_binary([Eol])},
                    {buff_limit, Limit}]),
    {ok, no_value, hibernate}.

%% @private @doc Handle call requiests.
-spec handle_call(Request :: term(),
                  From    :: from(),
                  State   :: state()) -> {reply, any(), state(), hibernate}.
handle_call(Request, From, State) ->
    lager:warning("unexpected call from ~p: ~p",
                  [From, Request]),
    {reply, {error, not_supported}, State, hibernate}.

%% @private @doc Handle cast requiests.
-spec handle_cast(Request :: term(),
                  State   :: state()) -> {noreply, state(), hibernate}.
handle_cast(Request, State) ->
    lager:warning("unexpected cast: ~p",
                  [Request]),
    {noreply, State, hibernate}.

%% @private @doc Handle other messages received by the process.
-spec handle_info(Info  :: term(),
                  State :: state()) -> {noreply, state(), hibernate} |
                                       {stop, term(), state()}.
handle_info({'EXIT', _Pid, normal}, State) ->
    %% Ignore this event
    {noreply, State, hibernate};
handle_info({'EXIT', _Pid, Reason}, State) ->
    %% A process we were linked to crashed
    {stop, Reason, State};
handle_info(Info, State) ->
    lager:warning("unexpected message: ~p",
                  [Info]),
    {noreply, State, hibernate}.

%% @private @doc Clean up after the server.
-spec terminate(Reason :: term(),
                State  :: state()) -> ok.
terminate(_Reson, _State) ->
    ranch:stop_listener(?ref),
    ok.

%% @private @doc Perform a code change. There are no older versions of
%% this module from which an upgrade would be possible, so this
%% function is just an empty stub.
-spec code_change(OldVsn :: term(),
                  State  :: state(),
                  Extra  :: term()) -> {ok, state()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
