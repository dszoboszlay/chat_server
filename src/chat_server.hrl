%%% Common settings for all modules in the chat_server

-compile([{parse_transform, lager_transform}]).
%% Use lager's parse transform.

-define(conn_cnt, [chat, connection, count]).
%% ID of the connection count metric.

-define(conn_err, [chat, connection, errors]).
%% ID of the connection error metric.

-define(msg_per_sec, [chat, msg_per_sec]).
%% ID of the incoming message/sec metric.

-define(msg_delivery, [chat, msg_delivery]).
%% ID of the message deliver time metric.
