%%% @doc A registry for keeping track of all active session
%%% processes. The registry is implemented as an ETS table. The key of
%%% the table is the socket, but it also stores the transport callback
%%% module and the pid of the session process.
%%%
%%% The registry is optimised for iterating over all the sessions in
%%% an unspecified order. Multiple iterations can run in parallel, and
%%% even {@link add_session/3}-{@link remove_session/1} call happens
%%% in the meantime. The iterator must be prepared to encounter
%%% sessions that are no longer live, but haven't yet been removed
%%% from the registry.
%%%
%%% The registry is owned by a process that can be started with {@link
%%% start_link/0}. Terminating this process also destroys the
%%% registry. The process takes care of periodically erasing crashed
%%% session processes from the registry. (Normally a session process
%%% should remove itself before terminating, but it's better to be
%%% safe than sorry.)
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(session_registry).

%% API
-export([start_link/0,
         add_session/3,
         remove_session/1,
         foreach/1
        ]).

-behaviour(gen_fsm).
-export([init/1,
         idle/2,
         idle/3,
         cleaning/2,
         cleaning/3,
         handle_event/3,
         handle_sync_event/4,
         handle_info/3,
         terminate/3,
         code_change/4
        ]).

-include("chat_server.hrl").

%%------------------------------------------------------------------------------
%% Constants
%%------------------------------------------------------------------------------

-define(tab, ?MODULE).
%% The name of the ETS table.

-ifdef(TEST).
-define(cleaning_period, timer:seconds(10)).
-else.
-define(cleaning_period, timer:hours(1)).
-endif.
%% The cleaning period of the FSM.

%%------------------------------------------------------------------------------
%% Types
%%------------------------------------------------------------------------------

-type state() :: no_value                       % In idle state
               | pid().                         % In cleaning state
%% The state data of the FSM.

-type state_name() :: idle
                    | cleaning.
%% The possible states of the FSM.

-type from() :: {pid(), _}.
%% The sender of a sync event.

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Start the process owning the registry.
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_fsm:start_link({local, ?MODULE}, ?MODULE, no_value, []).

%% @doc Add a new session process to the registry.
-spec add_session(Transport :: module(),
                  Socket    :: any(),
                  Pid       :: pid()) -> true.
add_session(Transport, Socket, Pid) ->
    ets:insert(?tab, [{Transport, Socket, Pid}]).

%% @doc Delete a session process from the registry.
-spec remove_session(Socket :: any()) -> true.
remove_session(Socket) ->
    ets:delete(?tab, Socket).

%% @doc Call a function on every session present in the registry.
-spec foreach(Fun :: fun((module(), any(), pid()) -> _)) -> ok.
foreach(Fun) ->
    ets:foldl(fun ({Transport, Socket, Pid}, Acc) ->
                      Fun(Transport, Socket, Pid),
                      Acc
              end,
              ok,
              ?tab).

%%------------------------------------------------------------------------------
%% gen_fsm callbacks
%%------------------------------------------------------------------------------

%% @private @doc Initialise the FSM.
-spec init(Args :: no_value) -> {ok, state_name(), state()}.
init(_Args) ->
    process_flag(trap_exit, true),
    ets:new(?tab,
            [set, public, named_table, {keypos, 2}, {read_concurrency, true}]),
    exometer:reset(?conn_cnt),
    exometer:reset(?conn_err),
    timer:apply_after(?cleaning_period, gen_fsm, send_event, [self(), clean]),
    {ok, idle, no_value}.

%% @private @doc Handle events in idle state.
-spec idle(Event :: term(), State :: state()) ->
                  {next_state, state_name(), state()}.
idle(clean, no_value) ->
    %% It's time to clean up the registry
    Pid = spawn_link(?MODULE, foreach, [fun cleaning_worker/3]),
    lager:debug("started cleaning worker ~p", [Pid]),
    {next_state, cleaning, Pid};
idle(Event, State) ->
    lager:warning("unexpected event in idle state: ~p", [Event]),
    {next_state, idle, State}.

%% @private @doc Handle sync events in idle state.
-spec idle(Event :: term(), From :: from(), State :: state()) ->
                  {reply, any(), state_name(), state()}.
idle(Event, From, State) ->
    lager:warning("unexpected event from ~p in idle state: ~p",
                  [From, Event]),
    {reply, {error, not_supported}, idle, State}.

%% @private @doc Handle events in cleaning state.
-spec cleaning(Event :: term(), State :: state()) ->
                      {next_state, state_name(), state()}.
cleaning(Event, State) ->
    lager:warning("unexpected event in cleaning state: ~p", [Event]),
    {next_state, cleaning, State}.

%% @private @doc Handle sync events in cleaning state.
-spec cleaning(Event :: term(), From :: from(), State :: state()) ->
                      {reply, any(), state_name(), state()}.
cleaning(Event, From, State) ->
    lager:warning("unexpected event from ~p in cleaning state: ~p",
                  [From, Event]),
    {reply, {error, not_supported}, cleaning, State}.

%% @private @doc Handle all state events.
-spec handle_event(Event     :: term(),
                   StateName :: state_name(),
                   State     :: state()) ->
                          {next_state, state_name(), state()}.
handle_event(Event, StateName, State) ->
    lager:warning("unexpected event in ~p state: ~p", [StateName, Event]),
    {next_state, StateName, State}.

%% @private @doc Handle sync events in cleaning state.
-spec handle_sync_event(Event     :: term(),
                        From      :: from(),
                        StateName :: state_name(),
                        State     :: state()) ->
                               {reply, any(), state_name(), state()}.
handle_sync_event(Event, From, StateName, State) ->
    lager:warning("unexpected event from ~p in ~p state: ~p",
                  [From, StateName, Event]),
    {reply, {error, not_supported}, StateName, State}.

%% @private @doc Handle other messages received by the process.
-spec handle_info(Info      :: term(),
                  StateName :: state_name(),
                  State     :: state()) ->
                         {stop, term(), state()} |
                         {next_state, state_name(), state()}.
handle_info({'EXIT', Pid, normal}, cleaning, Pid) ->
    %% Cleaning worker finished, schedule next run and return to idle
    %% state
    lager:debug("cleaning worker finished"),
    timer:apply_after(?cleaning_period, gen_fsm, send_event, [self(), clean]),
    {next_state, idle, no_value};
handle_info({'EXIT', Pid, Reason}, cleaning, Pid) ->
    %% Cleaning worker crashed, restart it immediately
    lager:debug("cleaning worker crashed: ~p", [Reason]),
    idle(clean, no_value);
handle_info({'EXIT', _Pid, normal}, StateName, State) ->
    %% Ignore this event
    {next_state, StateName, State};
handle_info({'EXIT', _Pid, Reason}, _StateName, State) ->
    %% A process we were linked to crashed
    {stop, Reason, State};
handle_info(Msg, StateName, State) ->
    lager:warning("unexpected message in ~p state: ~p", [StateName, Msg]),
    {next_state, StateName, State}.

%% @private @doc Clean up after the FSM.
-spec terminate(Reason    :: term(),
                StateName :: state_name(),
                State     :: state()) -> ok.
terminate(_Reson, _StateName, _State) ->
    ets:delete(?tab),
    ok.

%% @private @doc Perform a code change. There are no older versions of
%% this module from which an upgrade would be possible, so this
%% function is just an empty stub.
-spec code_change(OldVsn    :: term(),
                  StateName :: state_name(),
                  State     :: state(),
                  Extra     :: term()) -> {ok, state_name(), state()}.
code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%------------------------------------------------------------------------------
%% Internal helper functions
%%------------------------------------------------------------------------------

%% @doc Delete sessions from the registry where the process is no
%% longer alive.
-spec cleaning_worker(Transport :: module(),
                      Socket    :: any(),
                      Pid       :: pid()) -> _.
cleaning_worker(_Transport, Socket, Pid) ->
    is_process_alive(Pid) orelse
        begin
            exometer:update(?conn_err, +1),
            exometer:update(?conn_cnt, -1),
            ets:delete(?tab, Socket)
        end.
