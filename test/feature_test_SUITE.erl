%%% @hidden
%%% @doc Common Test suite for feature testing the chat server.
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(feature_test_SUITE).

-compile([export_all]).

%% CT callbacks
-export([all/0,
         init_per_suite/1,
         end_per_suite/1
        ]).

%% Test cases
-export([test_multi_user_chat/1,
         test_sequence_termination/1,
         test_control_msgs/1
        ]).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

%%------------------------------------------------------------------------------
%% CT callbacks
%%------------------------------------------------------------------------------

%% @doc List of all test cases in the suite. By convention a test case
%% is a function that takes 1 arguments and its name starts with
%% `test_'.
all() ->
    [Fun || {Fun, 1} <- module_info(exports),
            lists:prefix("test_", atom_to_list(Fun))].

%% @doc Initialise the test suite by starting a chat server node that
%% all subsequent tests will use.
init_per_suite(Config) ->
    {ok, Node} = test_lib:start_node(),
    {ok, _} = ct_cover:add_nodes([Node]),
    [{node, Node} | Config].

%% @doc Clean up after the test suite by stopping the test node.
end_per_suite(Config) ->
    %% Stop the test node
    Node = ?config(node, Config),
    ok = ct_cover:remove_nodes([Node]),
    ok = test_lib:stop_node(Node),
    ok.

%%------------------------------------------------------------------------------
%% Test cases
%%------------------------------------------------------------------------------

%% @doc Test that multiple users can talk to each other.
test_multi_user_chat(Config) ->
    Node = ?config(node, Config),

    %% Connect a single user and check it can send some messages, but
    %% doesn't receive anything from the server
    {P1, S1} = test_lib:connect(Node),
    ok = gen_tcp:send(S1, "hello world!\r"),
    ok = gen_tcp:send(S1, "is anybody here?\r"),

    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S1, 0, 0),

    %% Connect a second user and check two way communication
    {P2, S2} = test_lib:connect(Node),
    ok = gen_tcp:send(S1, "hello?\r"),
    ?assertEqual({ok, P1 ++ "hello?\r"}, gen_tcp:recv(S2, 0, 100)),
    ok = gen_tcp:send(S2, "hi there!\r"),
    ?assertEqual({ok, P2 ++ "hi there!\r"}, gen_tcp:recv(S1, 0, 100)),

    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S1, 0, 0),
    {error, timeout} = gen_tcp:recv(S2, 0, 0),

    %% Connect a third user and check multicasting
    {P3, S3} = test_lib:connect(Node),
    ok = gen_tcp:send(S3, "good day, everyone!\r"),
    ?assertEqual({ok, P3 ++ "good day, everyone!\r"}, gen_tcp:recv(S1, 0, 100)),
    ?assertEqual({ok, P3 ++ "good day, everyone!\r"}, gen_tcp:recv(S2, 0, 100)),
    ok = gen_tcp:send(S1, "we are all here\r"),
    ?assertEqual({ok, P1 ++ "we are all here\r"}, gen_tcp:recv(S2, 0, 100)),
    ?assertEqual({ok, P1 ++ "we are all here\r"}, gen_tcp:recv(S3, 0, 100)),

    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S1, 0, 0),
    {error, timeout} = gen_tcp:recv(S2, 0, 0),
    {error, timeout} = gen_tcp:recv(S3, 0, 0),

    gen_tcp:close(S1),
    gen_tcp:close(S2),
    gen_tcp:close(S3),
    ok.

% @doc Test that messages are only sent at line termination,
% regardless of the actual packets semt by the clients.
test_sequence_termination(Config) ->
    Node = ?config(node, Config),
    {P1, S1} = test_lib:connect(Node),
    {P2, S2} = test_lib:connect(Node),

    %% A message is only sent when the carriage return is sent
    ok = gen_tcp:send(S1, "I"),
    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S2, 0, 100),

    ok = gen_tcp:send(S1, " speak"),
    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S2, 0, 100),

    ok = gen_tcp:send(S1, " very"),
    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S2, 0, 100),

    ok = gen_tcp:send(S1, " slowly"),
    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S2, 0, 100),

    ok = gen_tcp:send(S1, "\r"),
    test_lib:wait_for_idle(Node),
    ?assertEqual({ok, P1 ++ "I speak very slowly\r"}, gen_tcp:recv(S2, 0, 100)),

    %% If multiple messages are sent in one packet they are all
    %% forwarded (in either one or in multiple packets, it doesn't
    %% matter)
    ok = gen_tcp:send(S2, "I speak very fast\r1\r2\r3\r4!\r"),

    Recv = P2 ++ "I speak very fast\r"
        ++ P2 ++ "1\r"
        ++ P2 ++ "2\r"
        ++ P2 ++ "3\r"
        ++ P2 ++ "4!\r",
    ?assertEqual({ok, Recv}, gen_tcp:recv(S1, length(Recv), 100)),

    gen_tcp:close(S1),
    gen_tcp:close(S2),
    ok.

%% @doc Test control messages.
test_control_msgs(Config) ->
    Node = ?config(node, Config),
    {_P1, S1} = test_lib:connect(Node),
    {_P2, S2} = test_lib:connect(Node),

    %% Control messages are not forwarded to other clients
    ok = gen_tcp:send(S1, "\\hello world"),
    test_lib:wait_for_idle(Node),
    {error, timeout} = gen_tcp:recv(S2, 0, 100),

    gen_tcp:close(S1),
    gen_tcp:close(S2),
    ok.
