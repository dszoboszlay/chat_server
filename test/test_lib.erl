%%% @hidden
%%% @doc Utility functions for testing
%%% @copyright 2014 Dániel Szoboszlay <dszoboszlay@gmail.com>
-module(test_lib).

%% API
-export([start_node/0,
         stop_node/1,
         connect/1,
         msg_prefix/1,
         wait_for/1, wait_for/2, wait_for/3,
         wait_for_idle/1
        ]).

%%------------------------------------------------------------------------------
%% Constants
%%------------------------------------------------------------------------------

-define(proj_root, "../..").
%% Relative path to the proect's root

-define(start_script,
        ?proj_root "/_rel/chat_server_release/bin/chat_server_release").
%% Relative path to the start script

%%------------------------------------------------------------------------------
%% Types
%%------------------------------------------------------------------------------

-type wait_cond(T) :: fun(() -> false | T).
%% A function to be used as the argument of {@link wait/1} and {@link
%% wait/2}.

-export_type([wait_cond/1
             ]).

%%------------------------------------------------------------------------------
%% API
%%------------------------------------------------------------------------------

%% @doc Start a chat server node. Optionally cover compile
-spec start_node() -> {ok, node()}.
start_node() ->
    %% Start the node with the start script
    {0, _} = eunit_lib:command(?start_script " start"),
    Node = 'chat_server@127.0.0.1',
    %% Wait for the node to finish it's boot up
    erlang:set_cookie(Node, chat_server),
    true = wait_for(fun () ->
                            rpc:call(Node, init, get_status, []) =:=
                                {started, started}
                    end),
    {ok, Node}.

%% @doc Stop a previously started chat server node.
-spec stop_node(Node :: node()) -> ok.
stop_node(_Node) ->
    {0, _} = eunit_lib:command(?start_script " stop"),
    ok.

%% @doc Connect a new chat user to the server node. Returns the
%% default message prefix from this user and the socket. The socket is
%% set up in passive mode, returning data as string.
-spec connect(Node :: node()) -> {binary(), inet:socket()}.
connect(Node) ->
    [_, Host] = string:tokens(atom_to_list(Node), "@"),
    {ok, Socket} = gen_tcp:connect(Host, 6667, [list, {active, false}]),
    {ok, {Address, Port}} = inet:sockname(Socket),
    DefName = string:join([integer_to_list(N) || N <- tuple_to_list(Address)],
                          ".")
        ++ ":" ++ integer_to_list(Port),
    ct:log("Connected user ~s", [DefName]),
    {msg_prefix(DefName), Socket}.

%% @doc Get the message prefix for a user name. All messages sent by
%% this user are expected to be prefixed with this value when
%% received.
-spec msg_prefix(Name :: iodata()) -> string().
msg_prefix(Name) ->
    "\e[32;1m" ++ unicode:characters_to_list(Name) ++ "\e[0m ".

%% @equiv wait_for(Cond, 100)
-spec wait_for(Cond :: wait_cond(T)) -> T.
wait_for(Cond) ->
    wait_for(Cond, 100).

%% @equiv wait_for(Cond, Wait, 10000 div Wait)
-spec wait_for(Cond :: wait_cond(T), Wait :: pos_integer()) -> T.
wait_for(Cond, Wait) ->
    Loops = timer:seconds(10) div Wait,
    wait_for(Cond, Wait, Loops).

%% @doc Wait for the condition to come by evaluating `Cond' every
%% `Wait' milliseconds until it returns something different from
%% `false', but at most `Loops' time.
-spec wait_for(Cond  :: wait_cond(T),
               Wait  :: pos_integer(),
               Loops :: non_neg_integer()) -> T.
wait_for(Cond, Wait, Loops) ->
    case Cond() of
        false when Loops < 1 ->
            ct:fail("Timeout in waiting for condition ~p", [Cond]);
        false ->
            timer:sleep(Wait),
            wait_for(Cond, Wait, Loops - 1);
        T ->
            T
    end.

%% @doc Wait for a chat server to become idle. The server is idle when
%% there are no message sendings in progress. However this function
%% cannot check the TCP connections are all idle too - it may happen
%% that a message packet already sent to the server hasn't yet arrived
%% when it returns.
-spec wait_for_idle(Node :: node()) -> true.
wait_for_idle(Node) ->
    wait_for(
      fun () ->
              %% Sending a message will fix the `session_registry'
              %% table, so the server is idle when this table is not
              %% fixed
              rpc:call(Node, ets, info, [session_registry, fixed])
                  =:= false
      end).
