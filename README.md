# Simple chat server

This project is the implementation of the following programming exercise in
Erlang:

_Chat server_

Write a robust server in Erlang or Java that listens for connecting clients on
tcp port 6667. All characters received from a client should be sent to all other
connected clients, unless the sequence starts with the character `$\`. A
sequence ends with carriage return. What the server does with sequences starting
with `$\` is up to you.
 
Your solution will be judged on robustness, capacity, features, and the beauty
of your code.

## User's guide

### Building the chat server

To build the project you will need OTP 17.0 or newer installed and
[rebar](https://github.com/rebar/rebar) available in your path. (The chat server
itself uses [erlang.mk](https://github.com/ninenines/erlang.mk), but not all of
the dependencies.)

To build and start the chat server simply type:
```
$ make
$ _rel/chat_server_release/bin/chat_server_release start
```

Later you can stop the server by typing:
```
$ _rel/chat_server_release/bin/chat_server_release stop
```

### Using the chat server

The easiest way to use the chat server is to connect with the `telnet`
program. Unfortunately the problem specification says _"a sequence ends with
carriage return"_; that is _CR_ and not _line feed (LF)_, or _CR LF_ (the
standard in the telnet protocol). There are two negative consequences of this
decision:

- When pressing Enter in the telnet client it will send a CR LF pair to the chat
  server. However, the chat server will terminate the sequence right after the
  CR and leave the LF in the buffer. The LF will be sent as the first character
  of the next sequence.
  
- When the telnet client receives the sole CR at the end of a message it will
  not start a new line. So you will overwrite the last message when you start
  typing your response.

    This second effect can be mitigated by turning on the `crmod` option:

```
$ telnet localhost 6667
^]set crmod
```

Alternatively you may edit the `sys.config` file and override the default
sequence terminator:

```erlang
[{chat_server,
  [{eol, $\n}
  ]}
].
```

### Special commands

Sequences starting with a backslash are not sent to other users but are executed
as commands. The following commands are supported:

- `\help`: print a help message.

- `\name NAME`: change the user's nick name.

- `\stats`: get performance statistics.

### Configuration options

In the `sys.config` file you can also change the default values of the following
configuration parameters:

Application | Parameter | Default | Description
---|---|---|---
`chat_server` | `ip` | `{0,0,0,0}` | The IP address on which to listen for incoming client connections.
`chat_server` | `port` | 6667 | The TCP port on which to listen for incoming client connections.
`chat_server` | `eol` | `$\r` | A character, string or binary that marks the end of the sequences in user input.
`chat_server` | `buff_limit` | 65536 | A soft limit on unsent message bytes to read ahead from the socket while waiting for the previous message(s) to be delivered.
`chat_server` | `acceptors` | 1000 | Number of `ranch` TCP acceptors.
`chat_server` | `debug` | `[]` | Debug options (like for a `gen_server`) for user connection processes.
`ranch` | `max_connections` | 10000 | Maximum number of parallel client connections.

### OS configuration tips

The following OS level settings are recommended:

- Raise the number of open files limit to at least 20000. The OS default is
  usually 1024, which means the chat server won't be able to handle more than
  1024 user connections.

### Performance metrics

The chat server provides information about its performance via `exometer`
metrics.

Metric | Type | Description
---|---|---
`[chat,connection,count]` | counter | Number of currently connected users.
`[chat,connection,errors]` | counter | Number of crashed connections cleaned up since the server was started.
`[chat,msg_delivery]` | histogram | Message delivery times over the last minute. Measured in **microseconds**.
`[chat,msg_per_sec]` | spiral | Number of messages received in the last second (one) and in total (count).

## Developer's guide

### Building & testing the chat server

The makefile of the project supports the following targets:

- `deps`: fetch dependencies.

- `app`: compile the code.

- `rel`: build a release in the `_rel` directory.

- `docs`: build EDoc documentation in the `doc` directory.

- `tests`: run CT test suites, placing the test logs into the `logs` directory.

- `plt`: build a `.plt` file for Dialyzer.

- `dialyze`: run Dialyzer on the project.

- `clean` and `distclean`: delete temporary and output files.

To build the release and run all tests it is recommended to type:
```
make docs tests dialyze
```

### Assumptions

These are the most important assumptions made during the development:

- The ordering of messages does not need to be the same on all connected
  clients. For example if both _A_ and _B_ send a message it may happen that _C_
  receives _A_'s message before _B_'s, but _D_ receives them in the opposite
  order.

    This assumption makes sense because maintaining a global ordering of
    messages would drastically reduce the scalability of the solution.

- The clients would send proper UTF-8 encoded messages. Strings generated by the
  server are always UTF-8 encoded, however the input received from the clients
  is not validated.

### Design goals

The main design goal was to create a scalable, robust solution. Thorough
testing, performance improvement and O&M features were prioritised over adding
new chat functionality.

### Architecture

A `ranch` TCP acceptor pool is used to receive client connections. The
`chat_session` module handles the actual connection. A new process is started
for each user.

This `chat_session` process takes care of the following things:

- Collecting entire sequences from the socket.

- Identifying control sequences (starting with a `$\\` character) and
  interpreting them.

- Prefixing all regular sequences with the user's name and handing them over to
  the `msg_delivery` module's worker for distribution to all other clients.

- While the worker is running, further sequences can be read and buffered from
  the socket (up to a limit to prevent turning all RAM to message buffers).

The user's default name contains her IP address and port number (because this is
a reasonably unique value). The name is always wrapped into some ANSI escape
code sequences to render it in bold green on terminal emulators.

Since the name is not allowed to contain control characters to separate the user
name from the actual message the client would only have to split each
(CR-terminated) line after the first occurrence of the `1b 5b 30 6d 20` bytes.

All `chat_session` processes register themselves via the `session_registry`
module. The registry itself is an ETS table that stores each socket together
with the corresponding `ranch` transport module and user process pid. The
`session_registry` module also implements a `gen_fsm` which is the owner of the
ETS table.

The `msg_delivery` module is meant to abstract away the strategy of distributing
messages to every connected client. Currently it is a very simple strategy: a
worker process is spawned that iterates over the registry table sending the
message to each socket (directly, not via the `chat_session` process).

When a user process terminates it needs to be removed from the
`session_registry` too. There are 3 mechanisms to accomplish that:

- If the `chat_session` terminates on its own (e.g. it doesn't crash) it will
  remove itself.

- When a `msg_delivery` worker encounters a closed socket in the registry, it
  removes the entry. This means a crashed session's entry will be cleaned up at
  the next message's delivery.

- In case there are no messages sent the `session_registry` FSM will
  periodically (yet rather infrequently) check all entries and remove those
  where the `chat_session` process has terminated.

### Planned improvements

- Add health checks to monitor critical parts of the VM: binary memory, slow
  ports and lengthy fixes on the session registry ETS table.
  
- Disconnect clients that cannot keep up receiving messages from the chat
  server.
  
    Since every message needs to be distributed to every other client a single
    user on a slow connection may slow down the entire server. A hostile client
    may even halt the chat server by deliberately not acknowledging packets for
    a long time etc.

    So the server needs a defence mechanism for disconnecting slow clients to
    provide some QoS for the rest of the users.
  
- Disconnect hostile clients that send too long sequences. Since the server
  needs to buffer data sent by a client until the sequence is terminated with a
  CR a hostile client may make the server run out of memory.

    A defence mechanism would be to disconnect clients sending too many
    characters without terminating the sequence.
  
- Allow distributed operation. Two chat servers could be connected via a TCP
  connection that acts almost like a regular `chat_session`: it forwards every
  message it receives to all other clients via `msg_delivery` and registers
  itself into the `session_registry` to receive all of the messages too.

    It seems to be more feasible to make this kind of point-to-point connection
    between two chat servers rather than making a shared registry. This way
    every message is moved only once between the two hosts, not as many times as
    many users are on the host.
  
- Write performance benchmarks.

- Make message delivery parallel by sharding user processes among _N_ session
  registries on an _N_ core host. Each registry fragment may be iterated over
  concurrently.
  
- Optimise VM options and application environment settings to improve
  throughput.
