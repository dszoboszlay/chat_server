PROJECT = chat_server

# Declare dependencies with specific tags. You don't want your
# dependency versions to change at random times.
DEPS = ranch lager exometer
dep_ranch    = git https://github.com/ninenines/ranch 1.1.0
dep_lager    = git https://github.com/basho/lager 2.0.3
dep_exometer = git https://github.com/Feuerlabs/exometer.git 1.0

# Exometer pulls in a lot of unnecessary dependencies that cause
# various problems in the build process (as they are not allways OTP
# 17 compatible). This option will remove most of them. However, the
# erlang.mk file had to be hacked too, so it won't call the default
# make target on exometer. The default target includes an xref test
# that would fail without all the dependencies in place.
deps: export EXOMETER_PACKAGES = "(minimal)"

# The CT tests operate on a release, so we need an extra dependency
tests-ct: rel
CT_OPTS = -name ct_$$$$@127.0.0.1 -cover test/ct.coverspec

# Set Dialyzer options
DIALYZER_OPTS = -Wrace_conditions

# The rest is handled by erlang.mk
include erlang.mk
